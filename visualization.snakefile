"""
Contains the following rules:
- create_pileup
- count_and_replace
- create_ggplot
- report

These are used to create results and to show a visualization of the results through e.g. a plot or a report
"""

rule create_pileup:
    input:
        "{basename}_filtered.bam"
    output:
        "results/pileup/{basename}_full.txt"
    message:
        "Creating a pileup file of {input}"
    log:
        "logs/pileup/{basename}.log"
    shell:
        "bbmap/pileup.sh in={input} out={output} 2> {log}"

rule count_and_replace:
    input:
        "results/pileup/{basename}_full.txt"
    output:
        "results/pileup/bp_per_pathogen/{basename}.txt"
    message:
        "Replacing accession numbers of {wildcards.basename}.txt with pathogen names and counting the present pathogens"
    script:
        "scripts/replace_accession_and_count_basepair.py"

rule create_ggplot:
    input:
        expand("results/pileup/bp_per_pathogen/{basename}.txt", basename=config['samples'])
    output:
        "results/plots/mean_pathogenfiles.pdf",
        "results/plots/mean_pathogenfiles_legend.pdf"
    message:
        "Creating plots using Rstudio"
    log:
        "logs/plot/ggplot.log"
    shell:
        "Rscript scripts/ggplot_without_unknown.R {input} {output[0]} {output[1]} > {log}"

rule report:
    input:
        pathogenfiles = expand("results/pileup/bp_per_pathogen/{basename}.txt", basename=config['samples']),
        plot = "results/plots/mean_pathogenfiles.pdf",
        plotlegend = "results/plots/mean_pathogenfiles_legend.pdf",
    output:
        "results/report.html"
    script:
        "scripts/report.py"
