configfile: "config.yaml"
include: "bowtie2.snakefile"
include: "samtools.snakefile"
include: "bedtools.snakefile"
include: "visualization.snakefile"

rule all:
    input:
        expand("results/accession_numbers/{basename}.txt", basename=config['samples']),
        "results/report.html"
