#!/usr/bin/python3

from snakemake.utils import report

pathogenAmount  = list()
filenames = list()

for sampleResult in snakemake.input[:-2]:
    pathogens = 0
    with open(sampleResult) as f:
        for line in f:
            pathogens += 1
    pathogenAmount.append(pathogens)
    filenames.append(sampleResult)

report("""
Pathogen analysis in brain samples of post mortem
==================================================

In this report an brain samples​ ​from​ ​the​ ​parietal​ ​cortex​ ​tissue​ ​and​ ​​epilepsy​ ​surgical samples 
​were​ ​taken​ ​postmortem​ ​to​ ​analyze​ ​the​ ​brain.​ ​
The​ ​test​ ​subjects​ ​are​ ​both​ ​males​ ​and​ ​females from​ ​Brazil,​ ​and​ ​the​ ​Netherlands​ ​of​ ​varying​ ​ages.

After mapping the brain samples against the human genome, foreign DNA was written to a new file, which was then mapped 
against a multifasta containing all the pathogens creating a .bam file.

This .sam file was then converted to .bam and filtered for the relevant information.
The information was then plotted using ggplot.

In total {} pathogens (see table pathogenfiles_ ) were found in {} brain samples.

This resulted in the plots (see tables plot_ and plotlegend_) which contains a visualization of the found pathogens.
""".format(sum(pathogenAmount), len(pathogenAmount)), snakemake.output[0], **snakemake.input, metadata="Author: Lin Chang (l.z.chang@st.hanze.nl)")
