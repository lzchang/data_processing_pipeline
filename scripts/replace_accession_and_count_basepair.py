#!/usr/bin/python3

"""
Program:	replace_accession.py
Version:	1.0
Author:		I.M. Gorter
Goal: 		Create a suitable file for the barplot from the output of the pileup 
Date: 		10-11-2017

"""

#imports
import glob
from os.path import basename


def basepair_per_pathogen(f):
	"""
	This function determines how many basepairs have been aligned per pathogen
	"""
	#basename of the file
	bn = basename(f)
	#creation of empty dictionary
	bpdict = {}
	
	openf = open(f, "r")
	
	for line in openf:
		#the pathogen is the first element
		pathogen = line.split("\t")[0]
		#the aligned basepairs is the sixt element
		basepair = line.split("\t")[5]
		#if the pathogen is not yet in the dictionary, add it with the basepairs
		if pathogen not in bpdict:
			bpdict[pathogen] = basepair
		#if the pathogen is in the dictionary, add the basepairs to the already existing basepairs	
		else:
			bpdict[pathogen] += basepair
			
	openf.close()		
	
	#open the outputfile
	newf = open(snakemake.output[0], "w")
	
	#for every pathogen-basepair combination, write to file
	for bp in bpdict:
		newf.write(bp + "\t" + str(bpdict[bp]) + "\n")

def remove_zero(f):
	"""
	This function removes the lines that have 0% coverage.
	When the fifth element is 0.0000, it means the coverage is 0%.
	"""
	#open the inputfile and read the lines
	openf = open(f, "r")
	lines = openf.readlines()

	openf.close()
	
	#open the inputfile as write
	openf = open(f, "w")
	for line in lines:
		#file is tab seperated, get all elements
		splitted = line.split("\t")
		#if the fifth element is 0.0000, coverage is 0%
		if splitted[4] == "0.0000":
			#dont write it back to the file
			pass
		else:
			#if the fifth element something else, write the line back
			openf.write(line)

	openf.close()


def get_Genomes():
	"""
	This function creates a dictionary that contains the accession number
	with the corresponding scientific name
	"""
	#multifasta that contains all organisms
	filenames = ['/data/storage/dataprocessing/all_bacteria.fna', '/data/storage/dataprocessing/all_viruses.fna']

	#Create empty dictionary
	genomedict = {}

	for fname in filenames:
		print("working on genomedict " + fname)
		with open(fname) as AllGenomes:
			for line in AllGenomes:
				#if the line startswith >gi, get the organism name between the |
				if line.startswith(">gi"):
					genome = line.split(">")[1].split(",")[0]
					refname = genome.split("| ")[0]
					organism = genome.split("| ")[1]
					#add accessionnumber and name to dictionary
					genomedict[refname] = ' '.join(organism.split(" ")[:2])
					
				#If the line startswitch something else, get the scientific name after the second space till the end
				elif line.startswith(">JPKZ") or line.startswith(">MIEF") or line.startswith(">LL") or line.startswith(">AWXF") or line.startswith("EQ") or line.startswith(">NW_") or line.startswith(">LWMK") or line.startswith(">NZ_") or line.startswith(">NC_") or line.startswith(">KT"):
					genome = line.split(">")[1].split(",")[0]
					refname = genome.split(" ")[0]
					organismName = genome.split(" ")[1:]
					organism = ' '.join(organismName)
					#add accessionnumber and name to dictionary
					genomedict[refname] = ' '.join(organism.split(" ")[:2])
		
	
	return genomedict

def replace_accession_with_name(f, genomedict):
	"""
	This function replaces the accessionnumber with the scientific name
	Uses the dictionary with the accessionumber and name from the get_Genomes() function
	"""
	#print("now working on file: " + f)
	#create empty list
	accession_nrs = []
	#get the basename of the file
	bn = basename(f)
	#open the file and read the lines
	openf = open(f, "r")
	lines = openf.readlines()[1:]
	openf.close()
	#open the file writeable
	newopenf = open(f, "w")
	
	for line in lines:
		#file is tab separated
		splitted = line.split("\t")[0][:-1]
		if splitted not in genomedict:
			pass
		else:
			#replace the accessionnumber with the scientific name that has the corresponding accessionnumber in the genomedict
			line = str(line).replace(str(splitted), str(genomedict[splitted]))
			#write to file
			newopenf.write(line)
			
def main():
	gd = get_Genomes()
	for f in snakemake.input:
		remove_zero(f)
		replace_accession_with_name(f, gd)
		basepair_per_pathogen(f)
	
if __name__ == "__main__":
	main()
