"""
Contains the rule get_accession_numbers use to obtain the accession numbers from the bam file
"""

rule get_accession_numbers:
    input:
        "{basename}_filtered.bam"
    output:
        "results/accession_numbers/{basename}.txt"
    message:
        "Getting the accesion numbers for {wildcards.basename}.bam"
    shell:
        "bedtools bamtobed -i {wildcards.basename}_filtered.bam > {output}"
