# Pathogen analysis in brain samples of post mortem 

In this report an brain samples​ ​from​ ​the​ ​parietal​ ​cortex​ ​tissue​ ​and​ ​​epilepsy​ ​surgical samples ​were​ ​taken​ ​postmortem​ ​to​ ​analyze​ ​the​ ​brain.​

The​ ​test​ ​subjects​ ​are​ ​both​ ​males​ ​and​ ​females from​ ​Brazil,​ ​and​ ​the​ ​Netherlands​ ​of​ ​varying​ ​ages.

Snakemake will produce a plot containing the percentages in which the pathogens appear in the samples, a report where these can be found and a file containing the found pathogens with the amount of occurrences.

## Usage 

1. Modify the config.yaml file
2. give the location of the genome file to filter on (organism) and the location of the genome to find matches with (pathogen) as the value.
3. Under samples enter your samples basename as key and the location as value (basename:location), excluding the extension of the file.
4. Change the extension of the reads if needed (default is .fastq) in the config.yaml
5. run Snakemake with the following:
```
snakemake
```

## Required software:

* Snakemake
* python3
* Bowtie2
* samtools
* bedtools
* Rstudio or similar for running R scripts

## Author
* Lin Chang @ l.z.chang@st.hanze.nl

