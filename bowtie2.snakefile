"""
Contains the following rules:
- build_indexes
- map_human_against_sample
- map_pathogen_against_filtered

These rules are used for building an bowtie2 index which is then used for mapping the samples
"""


rule build_indexes:
    input:
        lambda wildcards: config["refgenomes"][wildcards.indexName]
    output:
        temp(expand("indexes/{{indexName}}.{ind}.bt2", ind=range(1,5))),
        temp(expand("indexes/{{indexName}}.rev.{ind}.bt2", ind=range(1,3)))
    threads: 8
    log:
        "logs/build_indexes/{indexName}.log"
    message:
        "Building an index file for {wildcards.indexName}"
    shell:
        "bowtie2-build {input} indexes/{wildcards.indexName} -p {threads} > {log}"

rule map_human_against_sample:
    input:
        lambda wildcards: expand(config['samples'][wildcards.basename] + "_R{ind}" + config['ext'], ind=range(1,3)),
        expand("indexes/organism.{ind}.bt2", ind=range(1,5)),
        expand("indexes/organism.rev.{ind}.bt2", ind=range(1,3))
    output:
        temp("{basename}.1.fastq"),
        temp("{basename}.2.fastq"),
        temp("{basename}_temp.sam")
    threads : 32
    log:
        "logs/mapping/filter/{basename}.log"
    message:
        "Mapping {wildcards.basename} against organism index"
    shell:
        "bowtie2 -x indexes/organism -1 {input[0]} -2 {input[1]} --un-conc {wildcards.basename}.fastq -S {wildcards.basename}_temp.sam --no-unal --no-hd --no-sq -p {threads} 2>{log}"

rule map_pathogen_against_filtered:
    input:
        expand("{{basename}}.{ind}.fastq", ind=range(1,3)),
        expand("indexes/pathogen.{ind}.bt2", ind=range(1,5)),
        expand("indexes/pathogen.rev.{ind}.bt2", ind=range(1,3))
    output:
        temp("{basename}.sam")
    threads: 32
    log:
        "logs/mapping/findpathogens/{basename}.log"
    message:
        "Mapping {input[0]} and {input[1]} against pathogen index"

    shell:
        "bowtie2 -x indexes/pathogen -1 {input[0]} -2 {input[1]} -S {output} -p {threads} 2>{log}"
    
