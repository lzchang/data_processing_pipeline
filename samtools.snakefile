"""
Contains the following rules:
- convert_sam_to_bam
- filter_bam

These rules are used to process the .sam files obtained from mapping.
"""

rule convert_sam_to_bam:
    input:
        "{basename}.sam"
    output:
        temp("{basename}.bamtemp")
    threads: 8
    log:
        "logs/sam_to_bam/{basename}.log"
    message:
        "converting {input} to .bam file"
    shell:
        "samtools view -@ {threads} -b -S -o {output} {input} > {log}"

rule filter_bam:
    input:
        "{basename}.bamtemp"
    output:
        temp("{basename}_filtered.bam")
    threads: 8
    message:
        "filtering the bam file {input}"
    shell:
        "samtools view -@ {threads} -b -F 4 {input} > {output}"
